<?php

namespace MdAfzaran\Gridresponse;

use DateTime;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;
use phpDocumentor\Reflection\Types\Mixed_;

class ColumnProcessor
{

    /**
     * @param $columns
     * @return array|MessageBag
     * @throws ValidationException
     */
    public function isCorrectColumn($columns): array|MessageBag
    {
        $validated = [];
        foreach ($columns as $column)
        {
            $validator = $this->columnsValidation($column);
            if($validator->fails())
            {
                return $validator->errors();
            }
            $validated[] = $validator->validated();
        }
        return $validated;
    }

    /**
     * @param $columns
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function columnsValidation($columns): \Illuminate\Contracts\Validation\Validator
    {
        $rules = [
            'title' => 'required|string',
            'label' => 'required|string',
            'searchAble' => 'nullable|boolean',
            'sortAble' => 'nullable|boolean',
            'type' => 'in:string,float,integer,date',
            'width' => 'integer'
        ];

        $message = [
            'title.required' => 'title is required',
            'label.required' => 'label is required',
            'title.string' => 'title should be string',
            'searchAble.boolean' => 'this is not bool',
            'sortAble.boolean' => 'this is not bool',
            'type.column_type' => 'column type is not true',
            'width.integer' => 'the column should be integer'
        ];
        return Validator::make($columns,$rules,$message);
    }
}
