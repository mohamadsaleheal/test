<?php

namespace MdAfzaran\Gridresponse\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Column extends Model
{
    use HasFactory;
    protected $guarded= [];

    public function grid(): BelongsTo
    {
        return $this->belongsTo(Grid::class);
    }
}
