<?php

namespace MdAfzaran\Gridresponse\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Grid extends Model
{
    use HasFactory;
    protected $guarded= [];

    public function columns(): HasMany
    {
        return $this->hasMany(Column::class);
    }
}
