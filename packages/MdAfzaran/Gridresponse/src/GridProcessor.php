<?php

namespace MdAfzaran\Gridresponse;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use MdAfzaran\Gridresponse\Models\Grid;
use MdAfzaran\Gridresponse\Models\Column;
use phpDocumentor\Reflection\Types\Integer;

class GridProcessor implements GridInterface
{

    private array $columns = [];
    private Collection $columnsWithAllData;
    private string $gridSlug = '';
    private string $columnTitle = '';
    private string $columnProp = '';
    private string $columnPropValue = '';
    private array $errorMassage = [];
    private int $eachPageItemNumber = 10;
    private int $currentPage = 1;

    /**
     * @return Builder|Model
     */
    private function gridCreate(): Model|Builder
    {
        return Grid::query()->firstOrCreate([
            'slug'=>$this->gridSlug
        ]);
    }

    /**
     * @param $jsonData
     * @return Collection
     */
    private function dataFilter($jsonData): Collection
    {
        $arrayData = json_decode($jsonData,true);
        return collect($arrayData)->map(function ($item) {
            return collect((gettype($item) == 'array') ? $item : $item->toArray())
                ->only($this->columns)
                ->all();
        });
    }

    public function gridResponseGenerator($gridSlug,$columns): Model|array
    {
        $this->setColumns($columns);
        if(sizeof($this->errorMassage) !== 0)
            return $this->errorMassage;
        $this->setGridSlug($gridSlug);
        $grid = $this->gridCreate();
        $grid->columns()->saveMany($this->createColumns());
        return $grid;
    }

    public function showGrid($gridSlug,$jsonData): Collection
    {
        $returnData = [];
        $this->setGridSlug($gridSlug);
        $this->columnsWithAllData = $this->getColumns()->pluck('properties');
        $this->convertReadAbleColumns();
        $returnData['columns'] = $this->columnsWithAllData;
        $returnData['data'] = $this->dataFilter($jsonData)->forPage($this->currentPage,$this->eachPageItemNumber);
        return collect($returnData);
    }

    /**
     * @param $columns
     * @return void
     * @throws ValidationException
     */
    private function setColumns($columns): void
    {
        $result = (new ColumnProcessor())->isCorrectColumn($columns);
        if(gettype($result) === 'object')
            $this->errorMassage['message'] = $result->messages();
        else
            $this->columnsWithAllData = $result;
    }

    /**
     * @return void
     */
    private function convertReadAbleColumns()
    {
        $this->columnsWithAllData = $this->columnsWithAllData->map(function ($item){
            return json_decode($item,true);
        });
        $this->columns = $this->columnsWithAllData->pluck('title')->toArray();
    }

    /**
     * @param $gridSlug
     * @return GridProcessor
     */
    public function setGridSlug($gridSlug): GridProcessor
    {
        $this->gridSlug = $gridSlug;
        return $this;
    }

    /**
     * @return array
     */
    private function createColumns(): array
    {
        $columnsArray = [];
        foreach ($this->columnsWithAllData as $column)
        {
            $columnObj = new Column(['title'=>$column['title'],'properties'=>collect($column)->toJson()]);
            $columnsArray[] = $columnObj;
        }
        return $columnsArray;
    }

    /**
     * @param $columnProp
     * @param $columnPropValue
     * @return int
     */
    public function updateColumnPropProcessor($columnProp, $columnPropValue): int
    {
        $this->setColumnProp($columnProp,$columnPropValue);
        return $this->updateColumnProp();
    }


    /**
     * @param $columnProp
     * @param $columnPropValue
     * @return void
     */
    private function setColumnProp($columnProp, $columnPropValue): void
    {
        $this->columnProp = $columnProp;
        $this->columnPropValue = $columnPropValue;
    }

    /**
     * @param $column
     * @return GridProcessor
     */
    public function setColumn($column): GridProcessor
    {
        $this->columnTitle = $column;
        return $this;
    }

    /**
     * @return int
     */
    private function updateColumnProp(): int
    {
        $column = $this->getColumn($this->columnTitle);
        json_decode($column['properties'],true)[$this->columnProp] = $this->columnPropValue;
        return $column->update([
            'properties' => $column['properties']
        ]);
    }

    public function paginate($page,$numberOfEachPageItem): GridProcessor
    {
        $this->currentPage = $page;
        $this->eachPageItemNumber = $numberOfEachPageItem;
        return $this;
    }


    /**
     * @return Model
     */
    private function getGrid(): Model
    {
        return Grid::query()->where('slug',$this->gridSlug)->first();
    }


    /**
     * @param $columnTitle
     * @return mixed
     */
    private function getColumn($columnTitle): mixed
    {
        return $this->getColumns()->where('title',$columnTitle)->first();
    }

    /**
     * @return mixed
     */
    private function getColumns(): mixed
    {
        return $this->getGrid()->columns();
    }
}
