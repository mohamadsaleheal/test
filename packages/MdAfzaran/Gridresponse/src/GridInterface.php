<?php

namespace MdAfzaran\Gridresponse;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface GridInterface
{
    /**
     * @param $gridSlug
     * @param $columns
     * @return Model|array
     */
    public function gridResponseGenerator($gridSlug,$columns): Model|array;

    /**
     * @param $gridSlug
     * @param $jsonData
     * @return Collection
     */
    public function showGrid($gridSlug,$jsonData): Collection;

    /**
     * @param $columnProp
     * @param $columnPropValue
     * @return mixed
     */
    public function updateColumnPropProcessor($columnProp, $columnPropValue): mixed;

//    /**
//     * @return mixed
//     * method for set column title we want
//     */
//    public function setColumnTitle();
//
//
//    /**
//     * @return mixed
//     * method for set column label
//     */
//    public function setColumnLabel();
//
//    /**
//     * @return mixed
//     * method for set column width
//     */
//    public function setColumnSize();
//
//
//    /**
//     * @return mixed
//     * method for determine columns that can be search
//     */
//    public function setSearchAbleColumn();
//
//
//    /**
//     * @return mixed
//     * method for set sort able columns
//     */
//    public function setSortAbleColumn();
//
//    /**
//     * @return mixed
//     * method for determine column type
//     */
//    public function setColumnType();

    /**
     * @param $page
     * @param $numberOfEachPageItem
     * @return GridProcessor
     * method for set grid pagination
     */
    public function paginate($page,$numberOfEachPageItem): GridProcessor;
}
