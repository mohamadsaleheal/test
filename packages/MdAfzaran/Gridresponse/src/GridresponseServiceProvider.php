<?php

namespace MdAfzaran\Gridresponse;

use Illuminate\Support\ServiceProvider;

class GridresponseServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../migrations');
    }
}
