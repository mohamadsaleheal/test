<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransitionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric',
            'description' => 'required|alpha_num',
            'destinationFirstname' => 'required|alpha_num',
            'destinationLastname' => 'required|alpha_num',
            'destinationNumber' => 'required|alpha_num',
            'paymentNumber' => 'nullable',
            'reasonDescription' => 'nullable',
            'deposit' => 'nullable',
            'reasonDescription' => 'nullable',
            'sourceFirstName' => 'nullable',
            'sourceLastName' => 'nullable',
            'secondPassword' => 'nullable',
        ];
    }
}
