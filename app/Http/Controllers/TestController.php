<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use MdAfzaran\Gridresponse\GridProcessor;
use MdAfzaran\Gridresponse\Models\Grid;

class TestController extends Controller
{
    public function createGrid()
    {
        $columns = [
            ['title'=>'id','searchAble'=>true,'label'=>'آیدی','type'=>'integer'],
            ['title'=>'email','searchAble'=>true,'label'=>'ایمیل','type'=>'string'],
            ['title'=>'created_at','searchAble'=>true,'label'=>'ساخته شده','type'=>'date'],
        ];
        return (new GridProcessor())->gridResponseGenerator('nine',$columns);
    }

    public function showGrid()
    {
        $user = User::all();
        return (new GridProcessor())->paginate(1,4)->showGrid('nine',$user->toJson());
    }

    public function updateColumnProp(Request $request)
    {
        //here we can update all properties that we need or create that
        (new GridProcessor())->setGridSlug('nine')->setColumn($request['columnTitle'])
            ->updateColumnPropProcessor($request['propertiesName'],$request['propertiesValue']);
    }

    public function paginate()
    {

    }
}
